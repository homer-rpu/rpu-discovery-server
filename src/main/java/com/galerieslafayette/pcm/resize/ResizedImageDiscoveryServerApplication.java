package com.galerieslafayette.pcm.resize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by pierre on 02/02/18.
 */
@SpringBootApplication
@EnableEurekaServer
public class ResizedImageDiscoveryServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResizedImageDiscoveryServerApplication.class, args);
    }

}
